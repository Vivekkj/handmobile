/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, TextInput, View} from 'react-native';

import { SketchCanvas } from '@terrylinla/react-native-sketch-canvas';
import match from './src/match'
import PATTERNDATA from './src/malayalam.json'

type Props = {};
const THRESHOLD = 0.85;
const TIMEOUT = 1000;

export default class App extends Component<Props> {
  constructor () {
    super();

    this.state = {
      result: '',
      candidates: []
    };
  }

  cleanup = () => {
    this.canvas.clear();
  }

  onStrokeStart = () => {
    clearTimeout(this.timer);
  }

  onStrokeEnd = (data) => {
    this.timer = setTimeout(this.cleanup, TIMEOUT);

    if (data && data.path) {
      console.log('check for match');
      let cord = data.path.data.map((pos) => {
        pos = pos.split(',');

        let x = parseFloat(pos[0]);
        let y = parseFloat(pos[1]);
        return {x, y}
      });
      let results = match.run(cord, PATTERNDATA, THRESHOLD)

      if (results && results.length) {
        this.setState({
          result: this.state.result + results[0].pattern
        });
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.result}
          onChangeText={(result) => this.setState({result})}
          value={this.state.result}
          />
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <SketchCanvas
            ref={ref => this.canvas = ref}
            style={{ flex: 1 }}
            strokeColor={'red'}
            onStrokeStart={this.onStrokeStart}
            onStrokeEnd={this.onStrokeEnd}
            strokeWidth={7}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  result: {
    fontSize: 40,
    color: 'red'
  }
});
